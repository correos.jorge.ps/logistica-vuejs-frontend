import moment from "moment";

const CustomFormatters = {
  // eslint-disable-next-line
  install(Vue, options) {
    Vue.mixin({
      methods: {
        $formatDate(datestring) {
          // force locale to es. In next versions we have to implement
          // user lang selection or user brorwser locale
          moment.locale("es");
          return moment(String(datestring)).format("LLLL");
        }
      }
    });
  }
};

export default CustomFormatters;
