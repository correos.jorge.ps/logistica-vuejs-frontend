export const getApiUri = () => {};
import queryString from "query-string";

const CustomCov19Http = {
  // eslint-disable-next-line
  install(Vue, options) {
    Vue.mixin({
      methods: {
        $parseurl(uri, params = undefined, query = undefined) {
          let _uri = uri;
          if (params) {
            Object.keys(params).forEach(key => {
              _uri = _uri.replace("{" + key + "}", `${params[key]}`);
            });
          }
          if (query) {
            if (query) {
              const stringified = queryString.stringify(query);
              _uri = _uri + "?" + stringified;
            }
          }
          return _uri;
        },
        $_get_uri() {
          const hostname =
            process.env.VUE_APP_BACKEND_ADDRESS || window.location.hostname;
          let port = process.env.VUE_APP_BACKEND_PORT;
          const protocol = process.env.VUE_APP_BACKEND_PROTOCOL;
          const suffix = process.env.VUE_APP_BACKEND_API_SUFFIX;

          let uri = `${protocol}://${hostname}`;
          if (port && port.length > 0) {
            return `${uri}:${port}${suffix}`; // do not put ending slash
          }
          return `${uri}${suffix}`; // do not put ending slash
        },
        $_fetch(input, init = {}) {
          const uri = this.$_get_uri() + input;
          let headers = {
            Accept: "application/json",
            "Content-type": "application/json",
            "context-front": "l"
          };
          const jwt = localStorage.getItem("jwt_access");
          if (jwt) {
            // TODO validar jwt, si no valida, eliminar
            headers = {
              Authorization: "Bearer " + jwt,
              Accept: "application/json",
              "Content-type": "application/json",
              "context-front": "l"
            };
          } else {
            delete headers["Authorization"];
          }

          if (init.body) {
            init.body = JSON.stringify(init.body);
          }
          return fetch(
            uri,
            Object.assign(init, {
              headers: headers
            })
          );
        },
        $fetchget(input, init = {}) {
          const conf = Object.assign(init, { method: "GET" });
          return this.$_fetch(input, conf);
        },
        $fetchpost(input, init = {}) {
          const conf = Object.assign(init, { method: "POST" });
          return this.$_fetch(input, conf);
        },
        $fetchput(input, init = {}) {
          const conf = Object.assign(init, { method: "PUT" });
          return this.$_fetch(input, conf);
        },
        $fetchpatch(input, init = {}) {
          const conf = Object.assign(init, { method: "PATCH" });
          return this.$_fetch(input, conf);
        },
        $fetchdelete(input, init = {}) {
          const conf = Object.assign(init, { method: "DELETE" });
          return this.$_fetch(input, conf);
        }
      }
    });
  }
};

export default CustomCov19Http;
