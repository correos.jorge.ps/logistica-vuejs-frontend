import Vue from "vue";
import App from "./components/App.vue";
import { createRouter } from "./router";
import { createStore } from "@/store";
import VueRouter from "vue-router";
import Vuex from "vuex";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import CustomCov19Http from "./plugins/customhttp";
import CustomFormatters from "./plugins/formatters";
import ApiFormErrHandler from "./plugins/apiformerrhandler";

import _ from "lodash";
import "./assets/scss/main.scss";

Object.defineProperty(Vue.prototype, "$_", { value: _ });

// Launch vue.router
Vue.use(VueRouter);

// Launch Bootstrapvue
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

// Launch Vuex
Vue.use(Vuex);

// http
Vue.use(CustomCov19Http);
// formatters
Vue.use(CustomFormatters);
// form err feedback
Vue.use(ApiFormErrHandler);

import { mapState, mapActions } from "vuex";

export var store;
export function createApp() {
  const router = createRouter();
  store = createStore();

  const app = new Vue({
    data() {
      return {
        loadingapp: true
      };
    },
    computed: {
      ...mapState({
        availRegions: state => state.person.availRegions,
        person: state => state.person.person
      })
    },
    mounted: function() {
      this.initApp();
    },
    methods: {
      ...mapActions({
        LOAD_LOGISTIC_PERSON_REGIONS: "person/LOAD_LOGISTIC_PERSON_REGIONS",
        LOAD_LOGISTIC_PERSONS: "person/LOAD_LOGISTIC_PERSONS",
        LOADING_START: "apphealth/LOADING_START",
        LOADING_DONE: "apphealth/LOADING_DONE",
        SWITCH_LOGISTICS_PERSON: "person/SWITCH_LOGISTICS_PERSON"
      }),
      loadPrivateResources: function() {
        this.LOADING_START();
        if (!this.person) {
          localStorage.removeItem("pk_person");
          this.LOAD_LOGISTIC_PERSONS().then(
            res => {
              if (res.data.count <= 0) {
                this.LOADING_DONE();
                if (this.$route.name != "profile") {
                  this.$router.push({ name: "profile" });
                }
                return;
              }
              if (!localStorage.getItem("pk_person")) {
                this.LOADING_DONE();
                if (this.$route.name != "login") {
                  this.$router.push({ name: "login" });
                }
                return;
              }
              this.LOAD_LOGISTIC_PERSON_REGIONS().then(
                res => {
                  // no tiene regines
                  this.LOADING_DONE();
                  if (res.data.count <= 0) {
                    if (this.$route.name != "noRegions") {
                      this.$router.push({ name: "noRegions" });
                    }
                    return;
                  }
                  this.$router.push({ name: "dashboard" });
                },
                () => {
                  // error al cargar regiones, un 404 significa
                  // que no tiene regiones
                  this.LOADING_DONE();
                  if (this.$route.name != "noRegions") {
                    this.$router.push({ name: "noRegions" });
                  }
                  return;
                }
              );
            },
            () => {
              // error al obtener person desde la api
              this.LOADING_DONE();
            }
          );
        }
      },
      initApp: function() {
        if ((this.$route.meta || {}).auth) {
          this.loadPrivateResources();
        } else {
          this.LOADING_DONE();
        }
      }
    },
    router,
    store,
    ...App,
    render: h => h(App)
  });

  return { app, router, store };
}
