
const LosEstadosDeSolicitudes = [
    {cod: 0, name: 'Pendiente aceptar por técnico', class: 'text-warning'},
    {cod: 1, name: 'Aceptado, en estudio por técnico', class: 'text-dark'},
    {cod: 2, name: 'Presupuesto enviado, a la espera de respuesta de cliente', class: 'text-warning'},
    {cod: 3, name: 'Presupuesto aceptado por el cliente', class: 'text-success'},
    {cod: 4, name: 'Presupuesto rechazado por el técnico', class: 'text-danger'},
    {cod: 5, name: 'Presupuesto rechazado por el cliente', class: 'text-danger'}
    ] 

module.exports = LosEstadosDeSolicitudes;