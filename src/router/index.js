import Vue from "vue";
import Router from "vue-router";
import DefaultView from "@/views/DefaultView.vue";
import LoginView from "@/views/LoginView.vue";
import RegisterView from "@/views/RegisterView.vue";
import RegisterResendActivationLink from "@/views/RegisterResendActivationLink.vue";
import RecoverPassView from "@/views/RecoverPassView.vue";
import Dashboard from "@/components/Dashboard.vue";
import ProfileView from "@/views/ProfileView.vue";
import Stats from "@/components/Stats.vue";
import PersonView from "@/views/PersonView.vue";
import ShippingView from "@/views/ShippingView.vue";
import RequestView from "@/views/RequestView.vue";
import InventoryView from "@/views/InventoryView.vue";
import MovementView from "@/views/MovementView.vue";
import ConsumerPanelView from "@/views/ConsumerPanelView.vue";
import ProducerPanelView from "@/views/ProducerPanelView.vue";
import IntermediatePointsView from "@/views/IntermediatePointsView.vue";
import CarrierView from "@/views/CarrierView.vue";
import AutomatismsView from "@/views/AutomatismsView.vue";
import NoRegionsView from "@/views/NoRegionsView.vue";
import { ValidateJWT } from "@/utils/jwt.js";
import { store } from "../app";

Vue.use(Router);

export function createRouter() {
  const router = new Router({
    mode: "history",
    base: "/front",
    routes: [
      {
        path: "/",
        component: LoginView
      },
      {
        path: "/register",
        name: "register",
        component: RegisterView
      },
      {
        path: "/register/activate/:uid/:token",
        name: "register_activate",
        component: RegisterView,
        props: {
          activate: true
        }
      },
      {
        path: "/register/resendactivation",
        name: "resendactivation",
        component: RegisterResendActivationLink
      },
      {
        path: "/login",
        name: "login",
        component: LoginView
      },
      {
        path: "/recoverpass",
        name: "recoverpass",
        component: RecoverPassView
      },
      {
        path: "/recoverpass/confirm/:uid/:token",
        name: "recoverpass_confirm",
        component: RecoverPassView,
        props: { confirm: true }
      },
      {
        name: "defaultview",
        path: "/defaultview",
        component: DefaultView,
        children: [
          {
            path: "/dashboard",
            name: "dashboard",
            component: Dashboard,
            meta: {
              auth: true
            }
          },
          {
            path: "/noRegions",
            name: "noRegions",
            component: NoRegionsView
          },
          {
            path: "/stats",
            name: "stats",
            component: Stats,
            meta: {
              auth: true
            }
          },
          {
            path: "/consumerpanel",
            name: "consumerpanel",
            component: ConsumerPanelView,
            meta: {
              auth: true
            }
          },
          {
            path: "/producerpanel",
            name: "producerpanel",
            component: ProducerPanelView,
            meta: {
              auth: true
            }
          },
          {
            path: "/inventorylist",
            name: "inventorylist",
            component: InventoryView,
            meta: {
              auth: true
            }
          },
          {
            path: "/personlist",
            name: "personlist",
            component: PersonView,
            meta: {
              auth: true
            }
          },
          {
            path: "/shippinglist",
            name: "shippinglist",
            component: ShippingView,
            meta: {
              auth: true
            }
          },
          {
            path: "/movelist",
            name: "movelist",
            component: MovementView,
            meta: {
              auth: true
            }
          },
          {
            path: "/requestlist",
            name: "requestlist",
            component: RequestView,
            meta: {
              auth: true
            }
          },
          {
            path: "/profile",
            name: "profile",
            component: ProfileView,
            meta: {
              auth: true
            }
          },
          {
            path: "/intermediate",
            name: "intermediatePoints",
            component: IntermediatePointsView,
            meta: {
              auth: true
            }
          },
          {
            path: "/carriers",
            name: "carriers",
            component: CarrierView,
            meta: {
              auth: true
            }
          },
          {
            path: "/automatisms",
            name: "automatisms",
            component: AutomatismsView,
            meta: {
              auth: true
            }
          }
        ]
      }
    ]
  });

  const DEFAULT_TITLE = "Logística - CoronaVirusMakers";
  document.title = DEFAULT_TITLE;
  router.beforeEach((to, from, next) => {
    const needAuth = to.matched.some(route => route.meta.auth);

    // Vue.nextTick(() => {
    //   document.title = `${DEFAULT_TITLE} ${to.meta.title || ""}`;
    // });

    if (!needAuth) {
      // ruta abierta, no necesita JWT, continua
      next();
    } else {
      // ruta privada, necesita JWT válido
      if (!ValidateJWT(localStorage.getItem("jwt_access"))) {
        // borra JWT inválido
        localStorage.removeItem("jwt_access");
        // Intentamos refrescar el JWT contra la api. Si en el localstorage
        // no hay jwt_refresh no se llama a la api y la promesa se rechaza
        store.dispatch("auth/REFRESH_JWT").then(
          () => {
            // JWT refrescado, continua
            next();
            return;
          },
          () => {
            // JWT no refrescado, error en api? JWTRefresh erróneo?
            // envía a login.
            next({ name: "login" });
            return;
          }
        );
      } else {
        // JWT válido, continúa
        next();
      }
    }
  });

  return router;
}
