const formatOptions = {
  year: "numeric",
  month: "numeric",
  day: "numeric",
  hour: "numeric",
  minute: "numeric",
  second: "numeric",
  hour12: false,
  timeZone: "UTC"
};

const dateFormat = new Intl.DateTimeFormat("en-GB", formatOptions);
export const daySpan = 1000 * 60 * 60 * 24;

export function formatDate(date) {
  if (Number.isInteger(date) && date < 0) {
    date += Date.now();
  }
  const dateParts = dateFormat.formatToParts(date);
  const [
    { value: da },
    ,
    { value: mo },
    ,
    { value: ye },
    ,
    { value: ho },
    ,
    { value: mi },
    ,
    { value: se }
  ] = dateParts;
  return `${ye}-${mo}-${da}-${ho}:${mi}:${se}`;
}
