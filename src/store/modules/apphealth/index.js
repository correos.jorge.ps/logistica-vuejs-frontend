// import Vue from "vue";

export default {
  namespaced: true,
  state: {
    apploading: true,
    loadingerr: undefined
  },
  actions: {
    LOADING_START: ({ commit }) => {
      commit("MUTATE_APP_LOADING", true);
    },
    LOADING_DONE: ({ commit }) => {
      commit("MUTATE_APP_LOADING", false);
    }
  },
  mutations: {
    MUTATE_APP_LOADING: (state, value) => {
      state.apploading = value;
    }
  },
  getters: {}
};
