import Vue from "vue";
import CustomCov19Http from "@/plugins/customhttp";
import { ValidateJWT } from "@/utils/jwt.js";
Vue.use(CustomCov19Http);
let cross = new Vue();

export default {
  namespaced: true,
  state: {
    lastres: undefined
  },
  actions: {
    REGISTER: ({ commit }, payload) => {
      return new Promise((resolve, reject) => {
        cross
          .$fetchpost("/auth/users/", {
            body: {
              username: payload.username,
              password: payload.password,
              email: payload.email
            }
          })
          .then(res => {
            res.json().then(data => {
              if (res.ok) {
                commit("MUTATE_LAST_RES", res);
                resolve({ res, data });
              } else {
                commit("MUTATE_LAST_RES", res);
                reject({ res, data });
              }
            });
          });
      });
    },
    REGISTER_ACTIVATION: ({ commit }, payload) => {
      return new Promise((resolve, reject) => {
        cross
          .$fetchpost("/auth/users/activation/", {
            body: {
              uid: payload.uid,
              token: payload.token
            }
          })
          .then(res => {
            res.json().then(data => {
              if (res.ok) {
                commit("MUTATE_LAST_RES", res);
                resolve({ res, data });
              } else {
                commit("MUTATE_LAST_RES", res);
                reject({ res, data });
              }
            });
          });
      });
    },
    RESEND_ACTIVATION_LINK: ({ commit }, payload) => {
      return new Promise((resolve, reject) => {
        cross
          .$fetchpost("/auth/users/resend_activation/", {
            body: {
              email: payload.email
            }
          })
          .then(res => {
            res
              .json()
              .then(data => {
                if (res.ok) {
                  commit("MUTATE_LAST_RES", res);
                  resolve({ res, data });
                } else {
                  commit("MUTATE_LAST_RES", res);
                  reject({ res, data });
                }
              })
              .catch(() => {
                reject({ res, undefined });
              });
          });
      });
    },
    REFRESH_JWT: ({ commit }) => {
      return new Promise((resolve, reject) => {
        if (!localStorage.getItem("jwt_refresh")) {
          reject();
        }
        cross
          .$fetchpost("/auth/jwt/refresh/", {
            body: {
              refresh: localStorage.getItem("jwt_refresh")
            }
          })
          .then(res => {
            res.json().then(data => {
              if (res.ok) {
                if (ValidateJWT(data.access)) {
                  commit("MUTATE_JWT_ACCESS", data.access);
                  resolve({ res, data });
                } else {
                  commit("MUTATE_JWT_ACCESS", null);
                  reject({ res, data });
                }
              } else {
                commit("MUTATE_JWT_ACCESS", null);
                reject({ res, data });
              }
            });
          });
      });
    },
    LOGIN: ({ commit }, payload) => {
      return new Promise((resolve, reject) => {
        cross
          .$fetchpost("/auth/jwt/create/", {
            body: { username: payload.username, password: payload.password }
          })
          .then(res => {
            res.json().then(data => {
              if (res.ok) {
                if (ValidateJWT(data.access)) {
                  commit("MUTATE_JWT_ACCESS", data.access);
                  commit("MUTATE_JWT_REFRESH", data.refresh);
                  resolve({ res, data });
                } else {
                  commit("MUTATE_JWT_ACCESS", null);
                  commit("MUTATE_JWT_REFRESH", null);
                  reject({ res, data });
                }
              } else {
                commit("MUTATE_JWT_ACCESS", null);
                // TODO: necesitamos borrar el refresh si ha habido
                // un problema de conexión?
                commit("MUTATE_JWT_REFRESH", null);
                reject({ res, data });
              }
            });
          });
      });
    },
    LOGOUT: ({ commit }) => {
      return new Promise(resolve => {
        commit("MUTATE_JWT_ACCESS", null);
        commit("MUTATE_JWT_REFRESH", null);
        resolve();
      });
    },
    RESET_PASSWORD: ({ commit }, payload) => {
      return new Promise((resolve, reject) => {
        cross
          .$fetchpost("/auth/users/reset_password/", {
            body: {
              email: payload.email
            }
          })
          .then(res => {
            if (res.ok) {
              commit("MUTATE_LAST_RES", res);
              resolve({ res });
            } else {
              commit("MUTATE_LAST_RES", res);
              reject({ res });
            }
          });
      });
    },
    RESET_PASSWORD_CONFIRM: ({ commit }, payload) => {
      return new Promise((resolve, reject) => {
        cross
          .$fetchpost("/auth/users/reset_password_confirm/", {
            body: {
              uid: payload.uid,
              token: payload.token,
              new_password: payload.new_password
            }
          })
          .then(res => {
            res.json().then(data => {
              if (res.ok) {
                commit("MUTATE_LAST_RES", res);
                resolve({ res, data });
              } else {
                commit("MUTATE_LAST_RES", res);
                reject({ res, data });
              }
            });
          });
      });
    }
  },
  mutations: {
    MUTATE_LAST_RES: (state, res) => {
      state.lastres = res;
    },
    MUTATE_JWT_REFRESH: (state, token) => {
      if (!token) {
        return localStorage.removeItem("jwt_refresh");
      }
      localStorage.setItem("jwt_refresh", token);
    },
    MUTATE_JWT_ACCESS: (state, token) => {
      if (!token) {
        return localStorage.removeItem("jwt_access");
      }
      localStorage.setItem("jwt_access", token);
    }
  },
  getters: {}
};
