const path = require("path");
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
  .BundleAnalyzerPlugin;
const CopyWebpackPlugin = require("copy-webpack-plugin");
var conf = {
  name: "COV19 front"
};

let plugins = [
  new BundleAnalyzerPlugin({
    analyzerMode: "static",
    defaultSizes: "parsed",
    openAnalyzer: false
  })
];

// plugins["resolve"] = {
//   alias: {
//     "@": path.resolve(__dirname, "src/")
//   }
// };

module.exports = {
  publicPath: process.env.NODE_ENV === "production" ? "/front/" : "/",
  configureWebpack: {
    plugins,
    devtool: 'source-map'
  },
  css: {
    loaderOptions: {
      sass: {
        prependData: `
          @import "@/assets/scss/variables.scss";
          @import "@/assets/scss/mixins.scss";
          @import "@/../node_modules/bootstrap/scss/_functions.scss";
          @import "@/../node_modules/bootstrap/scss/_variables.scss";
          @import "@/../node_modules/bootstrap/scss/mixins/_breakpoints.scss";
        `
      }
    }
  },

  chainWebpack: (config) => {
    config.plugin("html").tap((args) => {
      args[0].title = conf.name;

      return args;
    });
    if (process.env.NODE_ENV === "production") {
      config.optimization.minimizer("terser").tap((args) => {
        args[0].terserOptions.ecma = 5;
        return args;
      });
    }
    // Only convert .svg files that are imported by these files as Vue component
    const FILE_RE = /\.(vue|js|ts|svg)$/;

    // Use vue-cli's default rule for svg in non .vue .js .ts files
    config.module.rule("svg").issuer((file) => !FILE_RE.test(file));

    // Use our loader to handle svg imported by other files
    config.module
      .rule("svg-component")
      .test(/\.svg$/)
      .issuer((file) => FILE_RE.test(file))
      .use("vue")
      .loader("vue-loader")
      .end()
      .use("svg-to-vue-component")
      .loader("svg-to-vue-component/loader");

    // Use images from assets in bootstrap-vue components
    config.module
      .rule("vue")
      .use("vue-loader")
      .loader("vue-loader")
      .tap((options) => {
        options.transformAssetUrls = {
          img: "src",
          image: "xlink:href",
          "b-img": "src",
          "b-img-lazy": ["src", "blank-src"],
          "b-card": "img-src",
          "b-card-img": "src",
          "b-card-img-lazy": ["src", "blank-src"],
          "b-carousel-slide": "img-src",
          "b-embed": "src"
        };
        return options;
      });
  },

  lintOnSave: undefined
};
